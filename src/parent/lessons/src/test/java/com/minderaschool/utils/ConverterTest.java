package com.minderaschool.utils;

import static org.junit.jupiter.api.Assertions.*;

class ConverterTest {

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert1() {
        assertEquals(1, Converter.romanToDecimal("I"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert2() {
        assertEquals(2, Converter.romanToDecimal("II"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert5() {
        assertEquals(5, Converter.romanToDecimal("V"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert4() {
        assertEquals(4, Converter.romanToDecimal("IV"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert10() {
        assertEquals(10, Converter.romanToDecimal("X"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert50() {
        assertEquals(50, Converter.romanToDecimal("L"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert100() {
        assertEquals(100, Converter.romanToDecimal("C"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert500() { assertEquals(500, Converter.romanToDecimal("D"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert1000() {
        assertEquals(1000, Converter.romanToDecimal("M"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert1556() {
        assertEquals(1556, Converter.romanToDecimal("MDLVI"));
    }
}