package com.minderaschool.Exc2;

public class App {
    public static void main(String[] args){
        Student person1 = new Student("Rui","Avenida dos aliados");
        Teacher person2 = new Teacher("Tiago","Rua das curvas");

        System.out.println();

        System.out.println(person1);
        System.out.println("Teacher: "+ person2);

        System.out.println();

        person1.addCourseGrade("matematica",15);
        person1.addCourseGrade("portugues",10);
        person1.addCourseGrade("ingles",9);

        System.out.println(person1.getAvarageGrade());

        person2.addCourses("PSI");
        person2.addCourses("MAT");
        person2.addCourses("ING");

        person2.printCourses();

        person2.removeCourses("MAT");

        person2.printCourses();
    }
}
