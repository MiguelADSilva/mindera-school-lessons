package com.minderaschool.Maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Hashtable;
import java.util.Map;

public class Exc1PedroVicente {
    public static void main(String[] args) {
        ArrayList<String> arrList = new ArrayList<String>() {{
            add("cão");
            add("gato");
            add("passaro");
            add("banana");
            add("morango");
            add("banana");
        }};

        //HasMap<String, Integer> map = new HasMap<String, Integer>(); // map sem ordem
        //LinkedHasMap<String, Integer> map = new LinkedHasMap<String, Integer>(); // map com ordem de implementação
        //TreeMap<String, Integer> map = new TreeMap<String, Integer>(); // map com ordem

        Hashtable<String, Integer> map = new Hashtable<String, Integer>(); // map com ordem

        for (int i = 0; i < arrList.size(); i++) {
            String key = arrList.get(i);
            if (map.containsKey(key)) {
                Integer x = map.get(key);
                x = x + 1;
                map.put(key, x);
            } else {
                map.put(key, 1);

            }

        }

        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
