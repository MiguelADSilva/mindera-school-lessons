package com.minderaschool.files;


import java.io.*;


public class FilesEx {

    public static void main(String[] args){
        File arquivo = new File("lessons/src/main/resources/MaxMin.txt");

        try{
            // arquivo.createNewFile();
            FileReader ler = new FileReader(arquivo);
            BufferedReader lerB = new BufferedReader(ler);

            FileWriter fileWriter = new FileWriter(arquivo, true);
            BufferedWriter escrever = new BufferedWriter(fileWriter);
            escrever.write("escrevendo");
            escrever.newLine();

            escrever.close();
            fileWriter.close();
            String linha = lerB.readLine();


            while (linha != null) {
                System.out.println(linha);
                linha = lerB.readLine();
            }

            File fil = new File("lessons/src/main/resources");
            File fi [] = fil.listFiles();
            for(int i = 0; i<fi.length;i++){
                System.out.println(fi[i]);

            }

        }catch(IOException ex){

        }
    }
}
