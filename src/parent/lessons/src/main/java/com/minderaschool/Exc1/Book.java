package com.minderaschool.Exc1;

public class Book{
    private String name;
    private Author author;
    private double price;

    public Book(String name, Author author, double price){
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void getAuthorData(){
        System.out.println("O autor chama-se " + author.getName() + " tem o sexo "+ author.getGender()+"o email é "+ author.getEmail());
    }
}
