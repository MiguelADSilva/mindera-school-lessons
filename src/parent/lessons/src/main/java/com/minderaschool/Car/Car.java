package com.minderaschool.Car;

public class Car {
    private Engine engine;
    private Tire[] tire;
    private Body body;
    private static final Logger logger = new Logger("Door");

    public Car(Engine engine, Tire[] tire, Body body){
        this.engine = engine;
        this.tire = tire;
        this.body = body;
    }

    public void move(){
        if(engine.isStarted()){
           for(int i = 0; i < tire.length; i++){
               tire[i].rotate();
           }
        }
    }

    public void openDoor(int door){
        if(!body.isOpen(door)){
            body.open(door);
        }else{
            logger.error("Door already open");
        }
    }

    public void closeDoor(int door){
        if(body.isOpen(door)){
            body.close(door);
        }else{
            logger.error("Door already close ");
        }
    }

    public void stop(){
        if(engine.isStarted() ){
            engine.stop();
        }
        System.out.println("Stopping car");
    }
}
