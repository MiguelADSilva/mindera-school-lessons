package com.minderaschool.inheritance;

public class Square extends Rectangle{
    double areaSquare, lado;

    public Square(double lado){
        this.lado = lado;
    }

    public double area(){
        areaSquare = lado * lado;
        return areaSquare;
    }
    public void WhoAmI(){
        super.name = "Square";
        super.WhoAmI();
    }

}
