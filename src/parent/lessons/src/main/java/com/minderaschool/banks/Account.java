package com.minderaschool.banks;

public class Account {
    private double balance;
    private String name;

    public Account(String name,double x){
        this.name = name;
        this.balance = x;
    }
    public void withdraw(double amount){
        this.balance -= amount;
    }
    public void deposit(double amount){
        this.balance += amount;
    }
    public void transfer(double amount, Account dest){
        this.withdraw(amount);
        dest.deposit(amount);
    }
    public double getBalance(){
        return this.balance;
    }
    public String getName(){
        return this.name;
    }

    public void transfer(String hugo, int i, String miguel) {
    }
}
