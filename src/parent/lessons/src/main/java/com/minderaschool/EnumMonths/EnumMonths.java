package com.minderaschool.EnumMonths;



public class EnumMonths{
    enum Months{
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER,
    
    }
    public static void main(String[] args){
        Months months = Months.JANUARY;

        switch(months){
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case OCTOBER:
            case DECEMBER:
                        System.out.println("Has has 31 days");
                        break;
            case FEBRUARY:
                        System.out.println("Has 28 days if it is a leap year it has 29 days");
                        break;
            case APRIL:
            case JUNE:
            case SEPTEMBER:
            case NOVEMBER:
                        System.out.println("Has 30 days");
                        break;
            default:
                    System.out.println("Repita PLEASEEEEEE!");
                    break;

        }
    }
}