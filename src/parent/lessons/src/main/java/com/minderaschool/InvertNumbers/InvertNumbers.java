package com.minderaschool.InvertNumbers;

import java.util.*;

public class InvertNumbers {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            //System.out.println("Enter the size of the array");
            int numArray = sc.nextInt();
            int[] array = new int[numArray];
            int[] temp = new int[numArray];
            int count = 0;

            do {
                temp[count] = sc.nextInt();
                count ++;
            }while (count < numArray);

            for(int i= 0; i<numArray; i++){
                array[i] = temp[(numArray - 1) - i];
            }
            for(int i= 0; i<array.length; i++){
                System.out.print(array[i]+ " ");
            }
        }
}
