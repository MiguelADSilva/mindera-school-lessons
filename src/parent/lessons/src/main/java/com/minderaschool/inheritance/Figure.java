package com.minderaschool.inheritance;

public class Figure {
    double areaValue;
    String name;

    public double area(){
        return areaValue;
    }
    public void WhoAmI(){
        System.out.println(name);
    }
}
