package com.minderaschool.Exc2;

public class Student extends Person {
    private int numCourses;
    private String[] courses;
    private int[] grades;


    public Student(String name, String address) {
        super(name, address);
        numCourses = 0;
        courses = new String[5];
        grades = new int[5];
    }

    @Override
    public String toString(){
        return "Student: " + super.toString();
    }

    public void addCourseGrade(String course, int grade){
        courses[numCourses] = course;
        grades[numCourses] = grade;
        numCourses++;
    }

    public void printGrades(){
        for(int i=0; i<numCourses; i++){
            System.out.print("Course: "+courses[i] + "Grade: "+grades[i]);
        }
        System.out.println();
    }

    public double getAvarageGrade(){
        int sum = 0;
        for (int i = 0; i < this.numCourses; i++){
            sum += grades[i];
        }
        return sum/this.numCourses;
    }

}
