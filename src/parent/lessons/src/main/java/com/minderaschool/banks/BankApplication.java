package com.minderaschool.banks;

public class BankApplication {
    public static void main(String[] args){
        Account Miguel = new Account("Miguel",2000);
        Miguel.withdraw(100);
        System.out.println(Miguel.getBalance());

        Account Hugo = new Account("Hugo",0);
        Hugo.deposit(1000);
        System.out.println(Hugo.getBalance());

        Miguel.transfer("Miguel",500, "Hugo");
        System.out.println("Account A- " + Miguel.getBalance());
        System.out.println("Account B+ "  + Hugo.getBalance());

        Hugo.transfer("Hugo",1000, "Miguel");
        System.out.println("Account B- " + Hugo.getBalance());
        System.out.println("Account A+ " + Miguel.getBalance());
    }
}
