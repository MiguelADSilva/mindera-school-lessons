package com.minderaschool.inheritance;

public class Triangle extends Figure{
    double areaTriangle;
    double base;
    double altura;

    public Triangle(double base, double altura){
        this.base = base;
        this.altura = altura;
    }

    public double area(){
        areaTriangle = (base-altura)/2;
        return areaTriangle;
    }
    public void WhoAmI(){
        super.name = "Triangle";
        super.WhoAmI();
    }
}
