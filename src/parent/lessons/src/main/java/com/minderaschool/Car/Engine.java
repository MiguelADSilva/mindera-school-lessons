package com.minderaschool.Car;

public class Engine {
    private boolean started;

    public void start(){
        this.started = true;
    }

    public void stop(){
        this.started = false;
    }

    public boolean isStarted(){
        return started;
    }
}
