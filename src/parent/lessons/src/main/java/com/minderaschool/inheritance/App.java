package com.minderaschool.inheritance;

public class App {
    public static void main(String[] args){
        Figure figure = new Figure();
        System.out.println("Figure area"+ figure.areaValue);
        figure.WhoAmI();
        System.out.println();

        Rectangle rectangle = new Rectangle(2,4);
        System.out.println("Rectangle area" + rectangle.areaRectangle);
        rectangle.WhoAmI();
        System.out.println();

        Circle circle = new Circle(3);
        System.out.println("Circle area"+ circle.raioCircle);
        circle.WhoAmI();
        System.out.println();

        Triangle triangle = new Triangle(3,5);
        System.out.println("Triangle"+ triangle.areaTriangle);
        triangle.WhoAmI();
        System.out.println();

        Square square = new Square(3);
        System.out.println("Square"+ square.areaSquare);
        square.WhoAmI();
        System.out.println();
    }
}
