package com.minderaschool.inheritanceCorrections;

public class Rectangle extends Figure{

    public Rectangle() { }

    public Rectangle(double sideA, double sideB) {
        super.areaValue = sideA * sideB;
        super.iAm = "Rectangle";
    }

    public double area(){
        return super.areaValue;
    }
    public void whoAmI(){
        System.out.println(super.iAm);
    }
}
