package com.minderaschool.inheritanceCorrections;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args){
        ArrayList<Figure> list = new ArrayList<Figure>();

        list.add(new Circle(4));
        list.add(new Rectangle(4,4));
        list.add(new Square(4));
        list.add(new Triangle(4,5));

        for(int i = 0; i < list.size(); i++){
            list.get(i).whoAmI();
            System.out.println(list.get(i).area());
        }
    }
}
