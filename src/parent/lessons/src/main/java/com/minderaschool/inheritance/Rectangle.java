package com.minderaschool.inheritance;

public class Rectangle extends Figure{
    double areaRectangle;
    double altura;
    double base;
    public Rectangle(double altura, double base){
        this.altura = altura;
        this.base = base;
    }

    public Rectangle(){

    }

    public double area(){
        areaRectangle = base * altura;
        return areaRectangle;
    }
    public void WhoAmI(){
        // System.out.println("I am Rectangle :D");
        super.name = "Rectangle";
        super.WhoAmI();
    }
}
