package com.minderaschool.inheritanceCorrections;

public class Figure {
    double areaValue = 0.0;
    String iAm = "Figure";
    public double area(){
        return areaValue;
    }
    public void whoAmI(){
        System.out.println(iAm);
    }
}
