package com.minderaschool.inheritanceCorrections;

public class Triangle extends Figure{
    public Triangle(double base, double heigth) {
        super.areaValue = (base * heigth) /2;
        super.iAm = "Triangle";
    }

    public double area(){
        return super.areaValue;
    }
    public void whoAmI(){
        System.out.println(iAm);

    }
}
