package com.minderaschool.Car;

public class Door {
    private boolean opened;

    public boolean isOppened(){
        return opened;
    }

    public void open(){
        System.out.println("Door Open");
        this.opened = true;

    }

    public void close(){
        System.out.println("Door Close");
        this.opened = false;
    }

}
