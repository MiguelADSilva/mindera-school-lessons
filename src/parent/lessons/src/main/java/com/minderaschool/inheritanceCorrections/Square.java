package com.minderaschool.inheritanceCorrections;

public class Square extends Rectangle{
    public Square(double side) {
        super.areaValue = side * side;
        super.iAm = "Square";
    }

    public double area(){
        return super.areaValue;
    }
    public void whoAmI(){
        System.out.println(iAm);
    }
}
