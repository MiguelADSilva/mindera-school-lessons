package com.minderaschool.Car;

public class Body {
    private Door[] doors;

    public Body(int numDoors){
        doors = new Door[numDoors];
        for(int i = 0; i< doors.length; i++){
            doors[i] = new Door();
        }
    }

    public void open(int door){
        System.out.println("Door " + door);
        doors[door].open();
    }

    public void close(int door){
        System.out.println("Door " + door);
        doors[door].close();
    }

    public boolean isOpen(int door){
        System.out.println("isOpen"+ door);
        return doors[door].isOppened();
    }
}
