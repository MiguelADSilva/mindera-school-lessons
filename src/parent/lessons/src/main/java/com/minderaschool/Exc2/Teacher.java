package com.minderaschool.Exc2;

public class Teacher extends Person{
    private int numCourses;
    private String[] courses;

    public Teacher(String name, String address) {
        super(name, address);
        numCourses = 0;
        courses = new String[5];
    }

    public boolean addCourses(String course){
        if(numCourses == 5){
            return false;
        }
        for(int i = 0; i< numCourses; i++){
            if(courses[i].equals(course)){
                return false;
            }
        }
        courses[numCourses] = course;
        numCourses++;
        return true;
    }

    public boolean removeCourses(String course){
        if(numCourses == 0){
            return false;
        }
        for(int i = 0; i < numCourses; i++){
            if(courses[i].equals(course)){
                for(int j = i; j < numCourses - 1; j++){
                    courses[j] = courses[j+1];
                }
                numCourses--;
                return true;
            }
        }
        return false;
    }

    public void printCourses() {
        for (int i = 0; i < numCourses; i++) {
            System.out.printf("\n %s", courses[i]);
        }
        System.out.println();
    }



}
