package com.minderaschool.inheritanceCorrections;

public class Circle extends Figure{
    public Circle(double radius) {
        super.areaValue = Math.PI*(radius*radius);
        super.iAm = "Circle";
    }

    public double area(){
        return super.areaValue;
    }
    public void whoAmI(){
        System.out.println(super.iAm);
    }
}
