package com.minderaschool.Car;

public class Main {
    public static void main(String[] args){
        Engine engine = new Engine();
        Tire[] tires = new Tire[4];
        Body body = new Body(4);

        Car carro = new Car(engine,tires,body);
        carro.openDoor(0);
        carro.closeDoor(0);
        carro.move();
        carro.stop();
    }
}
